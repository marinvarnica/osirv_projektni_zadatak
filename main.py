'''
8. Algoritam za transformaciju postojece realne slike u animiranu sliku

Opis zadatka : Implemenentirati algoritam koji ce od ulazne slike napraviti njezinu
animiranu verziju. Preporuca se upotreba kombinacije bilateralnog filtera
i adaptivnog thresholdinga kako bi se stvorio efekt animacije
(dopusteno je koristiti i druge metode kao i postojece OpenCV/Python biblioteke).
Teoretski opisati svaku koristenu metodu te prikazati rezultate - animirane slike.
'''

import helper
import numpy as np
import cv2

# i) Priprema slika
'''
Ucitavanje svih slika iz mape "inputs" u listu "image" i imena slika u listu "image_name".
'''
IMAGE_PATH = "./inputs"

image, image_name = helper.load_images(IMAGE_PATH, 0)

gray_image = list(range(len(image)))
edge_image = list(range(len(image)))

color_image = list(range(len(image)))
cartoon_image = list(range(len(image)))

# ii) Rubovi
'''
Na ucitanim slikama se moraju pronaci rubovi kako bi se dobio "pravilan izgled" animirane verzije ucitane slike.
Za pronalazak ruba, koristio sam OpenCV funkciju adaptivnog thresholdinga.

Pomocu if/else naredbe, provjeram jesu li slike ucitane 2D (grayscale) ili 3D (color) zbog toga sto adaptivni
trashold funkcioira samo s 2D slikama. Ako su ucitane kao 2D ostaju iste, a ako su ucitanje kao 3D mijenjamo ih u 2D
(Gray Colorspace)

Koristim OpenCV funkciju medianBlur da smanjim kolicinu rubova
'''

for i in range(len(image)):
    if len(image[i].shape) == 2: #grayscale
        gray_image[i] = cv2.medianBlur(image[i], 5)
    elif len(image[i].shape) == 3: #color
        gray_image[i] = cv2.cvtColor(image[i], cv2.COLOR_BGR2GRAY)
        gray_image[i] = cv2.medianBlur(gray_image[i], 5)

for i in range(len(image)):
    edge_image[i] = cv2.adaptiveThreshold(gray_image[i], 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

# iii) Boje
'''
Na ucitanim se slikama takodjer moraju "promjeniti" boje kako bi se dobio "pravilan izgled" animirane verzije slike.
Kristio sam OpenCV funkciju bilateralnog filtera zato sto ta funkcija moze smanjiti nezeljeni sum, a da ocuva rubove.
'''

for i in range(len(image)):
    color_image[i] = cv2.bilateralFilter(image[i], 9, 350, 350)

# iv) Animirana verzija slike
'''
Kako bi se dobila animirana verzija slike. Pomocu OpenCV funkcije bitwise_and se dobije takva slika.
'''

for i in range(len(image)):
    cartoon_image[i] = cv2.bitwise_and(color_image[i], color_image[i], mask = edge_image[i])


# v) Spremanje slike
'''
Na kraju se animirana verzija slike spremi u mapu "outputs"
'''

for i in range(len(image)):
    helper.save_image(cartoon_image[i], image_name[i], append_string = "_cartoon")
